/**
 * 用来测试tiff文件。
 * 目前简单的计算输入文件夹中tiff图像的数量
 * @author wang
 *
 */
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TiffCounts {
	
public static class TiffMapper extends
			Mapper<BytesWritable, BytesWritable, Text, IntWritable> {

		private  IntWritable wlen = new IntWritable(1);
		private Text word = new Text();

		public void map(BytesWritable key, BytesWritable value, Context context)
				throws IOException, InterruptedException {
					String fname = new String(key.getBytes(),Charset.defaultCharset());
					System.out.println(fname);
					int length = value.getLength();
					System.out.println(length);
					word.set(fname);
					wlen.set(length);
					context.write(word, wlen);
			}
	}

	public static class TiffReducer extends
			Reducer<Text, IntWritable, Text, IntWritable> {
		
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			   int sum = 0;
			    for(IntWritable i : values)
			    {
			    	sum += i.get();
			    }
			    result.set(sum);
			    context.write(key, result);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "tiff count");
		job.setJarByClass(TiffCounts.class);
		job.setMapperClass(TiffMapper.class);
		job.setCombinerClass(TiffReducer.class);
		job.setReducerClass(TiffReducer.class);
		job.setInputFormatClass(com.w208.TiffInputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
