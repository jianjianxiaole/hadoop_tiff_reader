package com.w208;

import java.io.*;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;

/**
 * 自定义的输出定义
 * @author wang
 *
 */
public class TiffRecordReader extends RecordReader<BytesWritable, BytesWritable> {

	private String fname;
//	private long length;
	private BytesWritable key;
	private BytesWritable value;
	private FileSplit inputSplit;
	private boolean isFinished;
	private JobContext jobContext;
	
	public TiffRecordReader(final InputSplit inputSplit,
			final TaskAttemptContext taskAttemptContext) throws IOException,
			InterruptedException {
	    
		fname = new String();
//		length = 0;
		key = new BytesWritable();
		value = new BytesWritable();
		isFinished = false;
		this.inputSplit = null;
		initialize(inputSplit, taskAttemptContext);
	}
	
	/**
	 * 此处重载，一次读取整个文件
	 */
	@Override
	public void initialize(final InputSplit inputSplit,
			final TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		if (inputSplit instanceof FileSplit) {
			this.inputSplit = (FileSplit) inputSplit;
			this.jobContext = taskAttemptContext;
		} else {
			throw new IOException("Input split is not an instance of FileSplit");
		}
	}
	
	@Override
	public synchronized void close() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BytesWritable getCurrentKey() {
		// TODO Auto-generated method stub
		return this.key;
	}

	@Override
	public BytesWritable getCurrentValue() {
		// TODO Auto-generated method stub
		return this.value;
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		if (!isFinished && this.inputSplit != null)
		{
			//final FileSplit fileSplit = (FileSplit) inputSplit;
//			length = fileSplit.getLength();
			final Path path = inputSplit.getPath();
			final FileSystem fileSystem = path.getFileSystem(jobContext
					.getConfiguration());
			fname = path.getName();
			key.set(fname.getBytes(), 0, fname.getBytes().length);
			
			//读取文件内容
			FSDataInputStream fsInput =  fileSystem.open(path);
			byte[] buff = new byte[(int) inputSplit.getLength()];
			int result = fsInput.read(buff, 0, buff.length);
			
			value.set(buff, 0, buff.length);
			isFinished = true;
			return isFinished;
		}
		return false;
	}

	@Override
	public float getProgress() throws IOException {
		// TODO Auto-generated method stub
		if (isFinished)
			return 1.0f;
		else
			return 0f;
	}
}
