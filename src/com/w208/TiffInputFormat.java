package com.w208;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

public class TiffInputFormat extends FileInputFormat<BytesWritable,BytesWritable> {

	@Override
	protected boolean isSplitable(JobContext context, Path filename) {
		// TODO Auto-generated method stub
		//return super.isSplitable(context, filename);
		return false;
	}

	/**
	 * 返回自定义的输入块
	 */
	@Override
	public RecordReader<BytesWritable, BytesWritable> createRecordReader(
			InputSplit split, TaskAttemptContext context) throws IOException {
		// TODO Auto-generated method stub
		TiffRecordReader one = null;
		try {
			one = new TiffRecordReader(split, context);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return one;
	}

	
	/**
	 * 在此处过滤非.tiff的的文件 -- 测试中使用.png 
	 * 此处测试OK
	 */
	@Override
	protected List<FileStatus> listStatus(JobContext arg0) throws IOException {
		// TODO Auto-generated method stub
		List<FileStatus> list =  super.listStatus(arg0);
		for (final FileStatus fileStatus : list) {
			if (!fileStatus.getPath().getName().toLowerCase().endsWith(".png")) {
				list.remove(fileStatus);
			}
		}
		return list;
	}

	@Override
	protected void addInputPathRecursively(List<FileStatus> arg0,
			FileSystem arg1, Path arg2, PathFilter arg3) throws IOException {
		// TODO Auto-generated method stub
		super.addInputPathRecursively(arg0, arg1, arg2, arg3);
	}

}
